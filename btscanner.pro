TARGET = btscanner
TEMPLATE = app

QT += bluetooth
QT -= gui

CONFIG   += console
CONFIG   -= app_bundle

SOURCES = \
    main.cpp \
    bdv.cpp

HEADERS = \
    bdv.h
