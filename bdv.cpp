#include "bdv.h"

bdv::bdv(QObject *)
{
    discoveryAgent = new QBluetoothDeviceDiscoveryAgent();

    connect(discoveryAgent, SIGNAL(deviceDiscovered(QBluetoothDeviceInfo)), this, SLOT(addDevice(QBluetoothDeviceInfo)));
    connect(discoveryAgent, SIGNAL(finished()), this, SLOT(scanFinished()));
    connect(discoveryAgent, SIGNAL(error(QBluetoothDeviceDiscoveryAgent::Error)), this, SLOT(error(QBluetoothDeviceDiscoveryAgent::Error)));
}

bdv::~bdv()
{
    delete discoveryAgent;
}

void bdv::addDevice(QBluetoothDeviceInfo info)
{
    qDebug() << QString("%1 %2").arg(info.address().toString()).arg(info.name());
}

void bdv::startScan()
{
    discoveryAgent->start();
    qDebug() << "start scann";
}

void bdv::scanFinished()
{
    qDebug() << "end scann";
}

void bdv::error(QBluetoothDeviceDiscoveryAgent::Error error)
{
    if (error == QBluetoothDeviceDiscoveryAgent::PoweredOffError)
        qDebug() << "The Bluetooth adaptor is powered off, power it on before doing discovery.";
    else if (error == QBluetoothDeviceDiscoveryAgent::InputOutputError)
        qDebug() << "Writing or reading from the device resulted in an error.";
    else
        qDebug() << "An unknown error has occurred.";
}
