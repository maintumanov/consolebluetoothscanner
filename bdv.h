#ifndef BDV_H
#define BDV_H

#include <QObject>

#include <QtBluetooth>
#include <QDebug>

class bdv: public QObject
{
    Q_OBJECT
public:
    explicit bdv(QObject *parent = 0);
    ~bdv();

public slots:
    void addDevice(QBluetoothDeviceInfo info);
    void startScan();
    void scanFinished();
    void error(QBluetoothDeviceDiscoveryAgent::Error error);

private:
    QBluetoothDeviceDiscoveryAgent *discoveryAgent;
    QBluetoothLocalDevice *localDevice;


};

#endif // BDV_H
